# README #

### What is this repository for? ###

This is an application aiming at looking at statistics from one or more bridge tournaments. The primary purpose is analysis of your own play to discover your strengths and weaknesses.

### How do I get set up? ###

* Summary of set up
Built in Visual Studio 2015.
Later on msi installers might be added for installation on Windows platforms. For now, download source code and build or download and unpack any of the build zip packages.
Currently no plan on supporting other platforms than Windows.
* Configuration
Nothing yet. Will add choice later on to use SQL server or SQLite as data storage.
* Dependencies
.NET Framework 4.6.1.
Optional: Sql Server for data storage
* Nuget dependencies
NUnit framework
OxyPlot
SQLite
NSubstitute
* Database configuration
Default setting uses local file storage with SQLite 
Optionally add a connection string for SQL Server (not yet implemented).
* How to run tests
Each assembly contains its own nunit tests. Run with with your favorite test runner, in Visual Studio or stand alone. 
Since the tests are embedded they can be run for an installed version as well.
* Deployment instructions
Download
Open solution in Visual Studio
Build
Run project BridgeStatistics

