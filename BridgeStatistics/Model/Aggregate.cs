﻿namespace BridgeStatistics.Model
{
  /// <summary>
  /// Common base class for all aggregates.
  /// </summary>
  public abstract class Aggregate
  {
    public int Id { get; set; }
  }
}