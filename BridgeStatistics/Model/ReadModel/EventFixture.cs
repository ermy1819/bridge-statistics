namespace BridgeStatistics.Model.ReadModel
{
  using NUnit.Framework;

  internal class EventFixture
  {
    [Test]
    public void ShouldBeReadableWhenConvertingToString()
    {
      var sut = new EventFactory();
      var @event = new WriteModel.Event
                     {
                       Id = 5,
                       Name = "some name",
                     };
      Assert.That(sut.CreateEvent(@event).ToString(), Is.EqualTo(@event.Name));
    }

    [Test]
    public void ShouldUseWriteModelNameWhenGettingName()
    {
      var sut = new EventFactory();
      var @event = new WriteModel.Event
                     {
                       Id = 5,
                       Name = "some name",
                     };
      Assert.That(sut.CreateEvent(@event).Name, Is.EqualTo(@event.Name));
    }
  }
}