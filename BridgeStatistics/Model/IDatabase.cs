﻿namespace BridgeStatistics.Model
{
  using System.Collections.Generic;

  using WriteModel;

  public interface IDatabase
  {
    void Persist(Root data);

    IEnumerable<Player> GetAllPlayers();

    IEnumerable<Event> GetAllEvents();

    IEnumerable<Game> GetGames(Event @event);

    IEnumerable<int> GetEventIdsForPlayers(IEnumerable<int> playerIds);
  }
}