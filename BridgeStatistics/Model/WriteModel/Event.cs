﻿namespace BridgeStatistics.Model.WriteModel
{
  using System;

  public class Event : Aggregate
  {
    public DateTime EndDate { get; set; }
    public string Name { get; set; }
    public string Site { get; set; }
    public DateTime StartDate { get; set; }

    public string DisplayName =>
      $"{Name} - {Site} - {StartDate.ToShortDateString()} - {EndDate.ToShortDateString()}";
  }
}