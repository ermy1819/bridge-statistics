﻿namespace BridgeStatistics.Model.WriteModel
{
  public struct Contract : IValueObject
  {
    public Contract(int tricks, Suit suit, Risk risk)
    {
      Tricks = tricks;
      Suit = suit;
      Risk = risk;
    }

    public int Tricks { get; }
    public Suit Suit { get; }
    public Risk Risk { get; }

    public bool Pass => Tricks == 0;
  }
}