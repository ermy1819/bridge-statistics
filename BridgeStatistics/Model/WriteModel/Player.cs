﻿namespace BridgeStatistics.Model.WriteModel
{
  public class Player : Aggregate
  {
    public string Name { get; set; }
  }
}