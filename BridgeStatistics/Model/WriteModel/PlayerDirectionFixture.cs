namespace BridgeStatistics.Model.WriteModel
{
  using NUnit.Framework;

  public class PlayerDirectionFixture
  {
    [Test]
    public void ShouldBeIncreasingNumbersClockwiseWhenCheckingIntValueOfFixedDirections()
    {
      var start = (int)PlayerDirection.North;
      Assert.That((int)PlayerDirection.East, Is.EqualTo(start + 1));
      Assert.That((int)PlayerDirection.South, Is.EqualTo(start + 2));
      Assert.That((int)PlayerDirection.West, Is.EqualTo(start + 3));
    }
  }
}