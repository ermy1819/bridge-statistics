﻿namespace BridgeStatistics.Model.WriteModel
{
  public enum Risk
  {
    Undoubled,

    Doubled,

    Redoubled,
  }
}