﻿namespace BridgeStatistics
{
  using System.Windows;

  using BridgeStatistics.Model.ReadModel;
  using BridgeStatistics.Views;

  using Model.WriteModel;
  using ViewModels;

#pragma warning disable S110 // Inheritance tree of classes should not be too deep
  /// <summary>
  /// Interaction logic for MainWindow
  /// </summary>
  public partial class MainWindow : Window
#pragma warning restore S110 // Inheritance tree of classes should not be too deep
  {
    private WorkspaceVM dataContext_;
    public MainWindow()
    {
      InitializeComponent();
      DialogViewModel.DialogService = new DialogService();
      var root = new Root(new Database.Database());
      var filter = new Filter();
      DataContext = dataContext_ = new WorkspaceVM(root, filter, new SelectionFactory().CreateSelection(root, filter));
    }
  }
}
