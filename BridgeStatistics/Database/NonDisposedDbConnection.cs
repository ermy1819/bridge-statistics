﻿namespace BridgeStatistics.Database
{
  using System.Data;
  using System.Data.Common;

  internal class NonDisposedDbConnection : DbConnection
  {
    private readonly DbConnection wrappedConnection_;

    public NonDisposedDbConnection(DbConnection wrappedConnection)
    {
      wrappedConnection_ = wrappedConnection;
    }

    public override string Database => wrappedConnection_.Database;

    public override ConnectionState State => wrappedConnection_.State;

    public override string DataSource => wrappedConnection_.DataSource;

    public override string ServerVersion => wrappedConnection_.ServerVersion;

    public override string ConnectionString
    {
      get
      {
        return wrappedConnection_.ConnectionString;
      }
      set
      {
        wrappedConnection_.ConnectionString = value;
      }
    }

    public override void Close()
    {      
      wrappedConnection_.Close();
    }

    public override void ChangeDatabase(string databaseName)
    {
      wrappedConnection_.ChangeDatabase(databaseName);
    }

    public override void Open()
    {
      wrappedConnection_.Open();
    }

    protected override DbCommand CreateDbCommand()
    {
      return wrappedConnection_.CreateCommand();
    }

    protected override DbTransaction BeginDbTransaction(IsolationLevel isolationLevel)
    {
      return wrappedConnection_.BeginTransaction(isolationLevel);
    }
  }
}