﻿namespace BridgeStatistics.Database
{
  using System;
  using System.Linq;

  using Model.WriteModel;

  using NUnit.Framework;

  [TestFixture(Dialect.Sqlite)]
  [TestFixture(Dialect.SqlServer2015)]
#pragma warning disable CA1812
  internal class DatabaseFixture
#pragma warning restore CA1812
  {
    private readonly Dialect dialect_;
    private readonly string connectionString_;

    public DatabaseFixture(Dialect dialect)
    {
      dialect_ = dialect;
      if (dialect == Dialect.SqlServer2015) connectionString_ = "Data Source=localhost;Initial Catalog=BridgeStatistics;Integrated Security=True;";
      else if (dialect == Dialect.Sqlite) connectionString_ = "Data Source=:memory:;Version=3;New=True;";
    }

    [SetUp]
    public void SetUp()
    {
      if (dialect_ == Dialect.SqlServer2015)
      {
        var database = new Database(dialect_, connectionString_);
        database.DropSchema();
      }
    }

    [Test]
    public void ShouldBeAbleToCreateSchema()
    {
      var sut = new Database(dialect_, connectionString_);
      sut.CreateSchema();
    }

    [Test]
    public void ShouldGetSameNameBackWhenRunningSinglePlayerRoundTrip()
    {
      var sut = new Database(dialect_, connectionString_);
      sut.CreateSchema();

      var data = new Root();
      data.Players.Add(0, new Player { Id = 0, Name = "Player name" });
      sut.Persist(data);

      var players = sut.GetAllPlayers();
      Assert.That(players.Single().Name, Is.EqualTo("Player name"));
    }

    [Test]
    public void ShouldGetSameNamesBackWhenRunningMultiplePlayerRoundTrip()
    {
      var sut = new Database(dialect_, connectionString_);
      sut.CreateSchema();

      var data = new Root();
      data.Players.Add(0, new Player { Id = 0, Name = "Player name" });
      data.Players.Add(1, new Player { Id = 1, Name = "John Doe" });
      data.Players.Add(2, new Player { Id = 2, Name = "Jane Doe" });
      sut.Persist(data);

      var players = sut.GetAllPlayers().ToList();
      Assert.That(players.Count, Is.EqualTo(data.Players.Count));
      Assert.That(players.Select(p => p.Name), Is.EquivalentTo(data.Players.Values.Select(p => p.Name)));
      Assert.That(players.Select(p => p.Id).Distinct().Count(), Is.EqualTo(data.Players.Count));
    }

    [Test]
    public void ShouldNotStoreDuplicateWhenPlayerAlreadyExistInDataSource()
    {
      var sut = new Database(dialect_, connectionString_);
      sut.CreateSchema();

      var data = new Root();
      data.Players.Add(0, new Player { Id = 0, Name = "Player name" });
      data.Players.Add(1, new Player { Id = 1, Name = "John Doe" });
      data.Players.Add(2, new Player { Id = 2, Name = "Jane Doe" });
      sut.Persist(data);

      var players1 = sut.GetAllPlayers().ToList();

      var data2 = new Root();
      data2.Players.Add(0, new Player { Id = 0, Name = "New Doe" });
      data2.Players.Add(1, new Player { Id = 1, Name = "Player name" });
      data2.Players.Add(2, new Player { Id = 2, Name = "Someone else" });
      sut.Persist(data2);

      var players2 = sut.GetAllPlayers().ToList();

      Assert.That(players1.Count, Is.EqualTo(data.Players.Count));
      var expectedPlayers = data.Players.Values.Select(p => p.Name).Union(data2.Players.Values.Select(p => p.Name)).ToList();
      Assert.That(players2.Count, Is.EqualTo(expectedPlayers.Count));
      Assert.That(players2.Select(p => p.Name), Is.EquivalentTo(expectedPlayers));
      Assert.That(players2.Select(p => p.Id).Distinct().Count(), Is.EqualTo(players2.Count));
      Assert.That(players1.All(p => p.Id == players2.Single(p2 => p2.Name == p.Name).Id));
    }

    [Test]
    public void ShouldGetSameEventBackWhenRunningSingleEventRoundTrip()
    {
      var sut = new Database(dialect_, connectionString_);
      sut.CreateSchema();

      var data = new Root();
      data.Events.Add(0, new Event { Id = 0, Name = "Event name", Site = "Here", StartDate = new DateTime(1999, 12, 24), EndDate = new DateTime(1999, 12, 26) });
      sut.Persist(data);

      var events = sut.GetAllEvents().ToList();
      Assert.That(events.Single().Name, Is.EqualTo(data.Events[0].Name));
      Assert.That(events.Single().Site, Is.EqualTo(data.Events[0].Site));
      Assert.That(events.Single().StartDate, Is.EqualTo(data.Events[0].StartDate));
      Assert.That(events.Single().EndDate, Is.EqualTo(data.Events[0].EndDate));
    }

    [Test]
    public void ShouldGetSameEventsBackWhenRunningMultipleEventsRoundTrip()
    {
      var sut = new Database(dialect_, connectionString_);
      sut.CreateSchema();

      var data = new Root();
      data.Events.Add(0, new Event { Id = 0, Name = "Event name", Site = "Here", StartDate = new DateTime(1999, 12, 24), EndDate = new DateTime(1999, 12, 26) });
      data.Events.Add(1, new Event { Id = 1, Name = "Event2", Site = "At home", StartDate = new DateTime(2009, 12, 24), EndDate = new DateTime(2010, 01, 02) });
      data.Events.Add(2, new Event { Id = 2, Name = "Tournament", Site = "Capital city", StartDate = new DateTime(2015, 02, 14), EndDate = new DateTime(2015, 02, 15) });
      sut.Persist(data);

      var events = sut.GetAllEvents().ToList();
      Assert.That(events.Count, Is.EqualTo(data.Events.Count));
      Assert.That(events.Select(p => (p.Name, p.Site, p.StartDate, p.EndDate)), Is.EquivalentTo(data.Events.Values.Select(p => (p.Name, p.Site, p.StartDate, p.EndDate))));
      Assert.That(events.Select(p => p.Id).Distinct().Count(), Is.EqualTo(data.Events.Count));
    }

    [Test]
    public void ShouldNotStoreDuplicateWhenEventAlreadyExistInDataSource()
    {
      var sut = new Database(dialect_, connectionString_);
      sut.CreateSchema();

      var data = new Root();
      data.Events.Add(0, new Event { Id = 0, Name = "Event name", Site = "Here", StartDate = new DateTime(1999, 12, 24), EndDate = new DateTime(1999, 12, 26) });
      data.Events.Add(1, new Event { Id = 1, Name = "Event2", Site = "At home", StartDate = new DateTime(2009, 12, 24), EndDate = new DateTime(2010, 01, 02) });
      data.Events.Add(2, new Event { Id = 2, Name = "Tournament", Site = "Capital city", StartDate = new DateTime(2015, 02, 14), EndDate = new DateTime(2015, 02, 15) });
      sut.Persist(data);

      var events1 = sut.GetAllEvents().ToList();

      var data2 = new Root();
      data2.Events.Add(0, new Event { Id = 0, Name = "different ", Site = "Here", StartDate = new DateTime(1999, 12, 24), EndDate = new DateTime(1999, 12, 26) });
      data2.Events.Add(1, new Event { Id = 1, Name = "Event name", Site = "????", StartDate = new DateTime(1999, 12, 24), EndDate = new DateTime(1999, 12, 26) });
      data2.Events.Add(2, new Event { Id = 2, Name = "Event name", Site = "Here", StartDate = new DateTime(1999, 12, 26), EndDate = new DateTime(1999, 12, 26) });
      data2.Events.Add(3, new Event { Id = 3, Name = "Event name", Site = "Here", StartDate = new DateTime(1999, 12, 24), EndDate = new DateTime(1999, 12, 27) });
      data2.Events.Add(4, new Event { Id = 4, Name = "Event name", Site = "Here", StartDate = new DateTime(1999, 12, 24), EndDate = new DateTime(1999, 12, 26) });
      sut.Persist(data2);

      var events2 = sut.GetAllEvents().ToList();

      Assert.That(events1.Count, Is.EqualTo(data.Events.Count));
      var expectedEvents = data.Events.Values.Select(p => (p.Name, p.Site, p.StartDate, p.EndDate)).Union(data2.Events.Values.Select(p => (p.Name, p.Site, p.StartDate, p.EndDate))).ToList();
      Assert.That(events2.Count, Is.EqualTo(expectedEvents.Count));
      Assert.That(events2.Select(p => (p.Name, p.Site, p.StartDate, p.EndDate)), Is.EquivalentTo(expectedEvents));
      Assert.That(events2.Select(p => p.Id).Distinct().Count(), Is.EqualTo(events2.Count));
      Assert.That(events1.All(p => p.Id == events2.Single(p2 => (p2.Name, p2.Site, p2.StartDate, p2.EndDate).Equals((p.Name, p.Site, p.StartDate, p.EndDate))).Id));
    }

    [Test]
    public void ShouldGetSameGameBackWhenRunningSingleGameRoundTrip()
    {
      var sut = new Database(dialect_, connectionString_);
      sut.CreateSchema();

      var data = new Root();
      data.Players.Add(0, new Player { Id = 0, Name = "East" });
      data.Players.Add(1, new Player { Id = 1, Name = "West" });
      data.Players.Add(2, new Player { Id = 2, Name = "North" });
      data.Players.Add(3, new Player { Id = 3, Name = "South" });
      data.Events.Add(0, new Event { Id = 0, Name = "Event name", Site = "Here", StartDate = new DateTime(1999, 12, 24), EndDate = new DateTime(1999, 12, 26) });
      data.Games.Add(0, new Game { Id = 0, Board = 7, Contract = new Contract(3, Suit.NoTrump, Risk.Undoubled), Date = new DateTime(2014, 07, 25), Deal = "123.123.123.1234", Declarer = PlayerDirection.North, EastId = 0, WestId = 1, NorthId = 2, SouthId = 3, EventId = 0, Vulnerable = Vulnerability.None, RawPbn = "a lot of text", Result = 9, Score = new Score(400), ScoreIMP = 2, ScoreMP = 4, ScorePercentage = 78, Scoring = "IMP" });
      sut.Persist(data);
      var @event = sut.GetAllEvents().First();
      var players = sut.GetAllPlayers().ToList();

      var game = sut.GetGames(@event).Single();
      Assert.That(game.Board, Is.EqualTo(data.Games[0].Board));
      Assert.That(game.Contract, Is.EqualTo(data.Games[0].Contract));
      Assert.That(game.Date, Is.EqualTo(data.Games[0].Date));
      Assert.That(game.Deal, Is.EqualTo(data.Games[0].Deal));
      Assert.That(game.Declarer, Is.EqualTo(data.Games[0].Declarer));
      Assert.That(game.EastId, Is.EqualTo(players.Single(p => p.Name == "East").Id));
      Assert.That(game.WestId, Is.EqualTo(players.Single(p => p.Name == "West").Id));
      Assert.That(game.NorthId, Is.EqualTo(players.Single(p => p.Name == "North").Id));
      Assert.That(game.SouthId, Is.EqualTo(players.Single(p => p.Name == "South").Id));
      Assert.That(game.EventId, Is.EqualTo(@event.Id));
      Assert.That(game.Vulnerable, Is.EqualTo(data.Games[0].Vulnerable));
      Assert.That(game.RawPbn, Is.EqualTo(data.Games[0].RawPbn));
      Assert.That(game.Result, Is.EqualTo(data.Games[0].Result));
      Assert.That(game.Score, Is.EqualTo(data.Games[0].Score));
      Assert.That(game.ScoreIMP, Is.EqualTo(data.Games[0].ScoreIMP));
      Assert.That(game.ScoreMP, Is.EqualTo(data.Games[0].ScoreMP));
      Assert.That(game.ScorePercentage, Is.EqualTo(data.Games[0].ScorePercentage));
      Assert.That(game.Scoring, Is.EqualTo(data.Games[0].Scoring));
    }

    [Test]
    public void ShouldGetReversedScoreWhenScoreIsForDefenders()
    {
      var sut = new Database(dialect_, connectionString_);
      sut.CreateSchema();

      var data = new Root();
      data.Players.Add(0, new Player { Id = 0, Name = "Player" });
      data.Events.Add(0, new Event { Id = 0, Name = "Event name", Site = "Here" });
      data.Games.Add(0, new Game { Id = 0, Declarer = PlayerDirection.North, RawPbn = "a lot of text", Score = new Score(400, PlayerDirection.East) });
      sut.Persist(data);
      var @event = sut.GetAllEvents().First();

      var game = sut.GetGames(@event).Single();
      Assert.That(game.Score, Is.EqualTo(new Score(-400)));
    }

    [Test]
    public void ShouldHandleNullsInNullableValuesWhenRunningGameRoundTrip()
    {
      var sut = new Database(dialect_, connectionString_);
      sut.CreateSchema();

      var data = new Root();
      data.Players.Add(0, new Player { Id = 0, Name = "East" });
      data.Events.Add(0, new Event { Id = 0, Name = "Event name", Site = "Here" });
      data.Games.Add(0, new Game { Id = 0, Contract = new Contract(), RawPbn = "" });
      sut.Persist(data);
      var @event = sut.GetAllEvents().First();

      var game = sut.GetGames(@event).Single();
      Assert.That(game.Date, Is.EqualTo(new DateTime(1900, 01, 01)));
      Assert.That(game.Deal, Is.EqualTo(null));
      Assert.That(game.RawPbn, Is.EqualTo(""));
      Assert.That(game.ScoreIMP, Is.EqualTo(null));
      Assert.That(game.ScoreMP, Is.EqualTo(null));
      Assert.That(game.ScorePercentage, Is.EqualTo(null));
      Assert.That(game.Scoring, Is.EqualTo(null));
    }

    [Test]
    public void ShouldSubstituteEventIdWithNewValueWhenPersistingGame()
    {
      var sut = new Database(dialect_, connectionString_);
      sut.CreateSchema();

      var data1 = new Root();
      data1.Events.Add(0, new Event { Id = 0, Name = "Event1", Site = "Here" });
      sut.Persist(data1);

      var data = new Root();
      data.Players.Add(0, new Player { Id = 0, Name = "East" });
      data.Events.Add(0, new Event { Id = 0, Name = "Event name", Site = "Here", StartDate = new DateTime(1999, 12, 24), EndDate = new DateTime(1999, 12, 26) });
      data.Games.Add(0, new Game { Id = 0, Contract = new Contract(), RawPbn = "", EventId = 0 });
      sut.Persist(data);
      var events = sut.GetAllEvents().ToList();
      var gameEvent = events.Single(e => e.Name == "Event name");

      var game = sut.GetGames(gameEvent).Single();
      Assert.That(gameEvent.Id, Is.Not.EqualTo(0));
      Assert.That(game.EventId, Is.EqualTo(gameEvent.Id));
    }

    [Test]
    public void ShouldSubstitutePlayerIdWithNewValueWhenPersistingGame()
    {
      var sut = new Database(dialect_, connectionString_);
      sut.CreateSchema();

      var data1 = new Root();
      data1.Players.Add(0, new Player { Id = 0, Name = "Player1" });
      sut.Persist(data1);

      var data = new Root();
      data.Players.Add(0, new Player { Id = 0, Name = "East" });
      data.Players.Add(1, new Player { Id = 1, Name = "West" });
      data.Players.Add(2, new Player { Id = 2, Name = "North" });
      data.Players.Add(3, new Player { Id = 3, Name = "South" });
      data.Events.Add(0, new Event { Id = 0, Name = "Event name", Site = "Here" });
      data.Games.Add(0, new Game { Id = 0, Contract = new Contract(), RawPbn = "", EastId = 0, WestId = 1, NorthId = 2, SouthId = 3 });
      sut.Persist(data);
      var gameEvent = sut.GetAllEvents().Single();
      var players = sut.GetAllPlayers().ToList();

      var game = sut.GetGames(gameEvent).Single();
      Assert.That(players.Single(p => p.Name == "East").Id, Is.Not.EqualTo(0));
      Assert.That(players.Single(p => p.Name == "West").Id, Is.Not.EqualTo(1));
      Assert.That(players.Single(p => p.Name == "North").Id, Is.Not.EqualTo(2));
      Assert.That(players.Single(p => p.Name == "South").Id, Is.Not.EqualTo(3));
      Assert.That(game.EastId, Is.EqualTo(players.Single(p => p.Name == "East").Id));
      Assert.That(game.WestId, Is.EqualTo(players.Single(p => p.Name == "West").Id));
      Assert.That(game.NorthId, Is.EqualTo(players.Single(p => p.Name == "North").Id));
      Assert.That(game.SouthId, Is.EqualTo(players.Single(p => p.Name == "South").Id));
    }

    [Test]
    public void ShouldNotStoreDuplicateGames()
    {
      var sut = new Database(dialect_, connectionString_);
      sut.CreateSchema();

      var data1 = new Root();
      data1.Players.Add(0, new Player { Id = 0, Name = "East" });
      data1.Players.Add(1, new Player { Id = 1, Name = "West" });
      data1.Players.Add(2, new Player { Id = 2, Name = "North" });
      data1.Players.Add(3, new Player { Id = 3, Name = "South" });
      data1.Events.Add(0, new Event { Id = 0, Name = "Event name", Site = "Here" });
      data1.Games.Add(0, new Game { Id = 0, Board = 4, Contract = new Contract(), RawPbn = "11111", EastId = 0, WestId = 1, NorthId = 2, SouthId = 3, EventId = 0 });
      sut.Persist(data1);

      var data = new Root();
      data.Players.Add(0, new Player { Id = 0, Name = "East" });
      data.Players.Add(1, new Player { Id = 1, Name = "West" });
      data.Players.Add(2, new Player { Id = 2, Name = "North" });
      data.Players.Add(3, new Player { Id = 3, Name = "South" });
      data.Events.Add(0, new Event { Id = 0, Name = "Event name", Site = "Here" });
      data.Games.Add(0, new Game { Id = 0, Board = 4, Contract = new Contract(), RawPbn = "", EastId = 0, WestId = 1, NorthId = 2, SouthId = 3, EventId = 0 });
      data.Games.Add(1, new Game { Id = 1, Board = 4, Contract = new Contract(), RawPbn = "Different", EastId = 0, WestId = 1, NorthId = 2, SouthId = 3, EventId = 0 });
      sut.Persist(data);

      var gameEvent = sut.GetAllEvents().Single();

      var game = sut.GetGames(gameEvent);
      Assert.That(game.Count(), Is.EqualTo(1));
    }

    [Test]
    public void ShouldAddGameWhenOnlyOnePartOfKeyIsDifferent()
    {
      // Also tests eventId filtering
      var sut = new Database(dialect_, connectionString_);
      sut.CreateSchema();

      var data = new Root();
      data.Players.Add(0, new Player { Id = 0, Name = "East" });
      data.Players.Add(1, new Player { Id = 1, Name = "West" });
      data.Players.Add(2, new Player { Id = 2, Name = "North" });
      data.Players.Add(3, new Player { Id = 3, Name = "South" });
      data.Players.Add(4, new Player { Id = 4, Name = "Extra" });
      data.Events.Add(0, new Event { Id = 0, Name = "Event name", Site = "Here" });
      data.Events.Add(1, new Event { Id = 1, Name = "Event name", Site = "There" });
      data.Games.Add(0, new Game { Id = 0, Board = 4, Contract = new Contract(), RawPbn = "", EastId = 0, WestId = 1, NorthId = 2, SouthId = 3, EventId = 0 });
      data.Games.Add(1, new Game { Id = 1, Board = 3, Contract = new Contract(), RawPbn = "", EastId = 0, WestId = 1, NorthId = 2, SouthId = 3, EventId = 0 });
      data.Games.Add(2, new Game { Id = 2, Board = 4, Contract = new Contract(), RawPbn = "", EastId = 4, WestId = 1, NorthId = 2, SouthId = 3, EventId = 0 });
      data.Games.Add(3, new Game { Id = 3, Board = 4, Contract = new Contract(), RawPbn = "", EastId = 0, WestId = 4, NorthId = 2, SouthId = 3, EventId = 0 });
      data.Games.Add(4, new Game { Id = 4, Board = 4, Contract = new Contract(), RawPbn = "", EastId = 0, WestId = 1, NorthId = 4, SouthId = 3, EventId = 0 });
      data.Games.Add(5, new Game { Id = 5, Board = 4, Contract = new Contract(), RawPbn = "", EastId = 0, WestId = 1, NorthId = 2, SouthId = 4, EventId = 0 });
      data.Games.Add(6, new Game { Id = 6, Board = 4, Contract = new Contract(), RawPbn = "", EastId = 0, WestId = 1, NorthId = 2, SouthId = 3, EventId = 1 });
      sut.Persist(data);

      var gameEvent = sut.GetAllEvents().ToList();

      Assert.That(sut.GetGames(gameEvent[0]).Count(), Is.EqualTo(data.Games.Values.Count(g => g.EventId == data.Events.Values.Single(e => e.Site == gameEvent[0].Site).Id)));
      Assert.That(sut.GetGames(gameEvent[1]).Count(), Is.EqualTo(data.Games.Values.Count(g => g.EventId == data.Events.Values.Single(e => e.Site == gameEvent[1].Site).Id)));
    }

    [Test]
    public void ShouldGetAllGamesForEventWhenNoPlayerFilterIsApplied()
    {
      var sut = new Database(dialect_, connectionString_);
      sut.CreateSchema();

      var data = new Root();
      data.Players.Add(0, new Player { Id = 0, Name = "East" });
      data.Players.Add(1, new Player { Id = 1, Name = "West" });
      data.Players.Add(2, new Player { Id = 2, Name = "North" });
      data.Players.Add(3, new Player { Id = 3, Name = "South" });
      data.Players.Add(4, new Player { Id = 4, Name = "Extra" });
      data.Events.Add(0, new Event { Id = 0, Name = "Event name", Site = "Here" });
      data.Events.Add(1, new Event { Id = 1, Name = "Event2", Site = "Here" });
      data.Games.Add(0, new Game { Id = 0, Contract = new Contract(), RawPbn = "Match0", EastId = 0, WestId = 1, NorthId = 2, SouthId = 3, EventId = 0 });
      data.Games.Add(1, new Game { Id = 1, Contract = new Contract(), RawPbn = "Match1", EastId = 0, WestId = 0, NorthId = 0, SouthId = 0, EventId = 1 });
      data.Games.Add(2, new Game { Id = 2, Contract = new Contract(), RawPbn = "Match0", EastId = 3, WestId = 0, NorthId = 2, SouthId = 3, EventId = 0 });
      data.Games.Add(3, new Game { Id = 3, Contract = new Contract(), RawPbn = "Match1", EastId = 1, WestId = 2, NorthId = 0, SouthId = 3, EventId = 1 });
      data.Games.Add(4, new Game { Id = 4, Contract = new Contract(), RawPbn = "Match0", EastId = 3, WestId = 1, NorthId = 2, SouthId = 0, EventId = 0 });
      data.Games.Add(5, new Game { Id = 5, Contract = new Contract(), RawPbn = "Match1", EastId = 4, WestId = 1, NorthId = 2, SouthId = 3, EventId = 1 });
      data.Games.Add(6, new Game { Id = 6, Contract = new Contract(), RawPbn = "Match0", EastId = 2, WestId = 1, NorthId = 2, SouthId = 3, EventId = 0 });
      sut.Persist(data);

      var gameEvent = sut.GetAllEvents().ToList();

      var games = sut.GetGames(gameEvent.Single(e => e.Name == "Event name")).ToList();
      Assert.That(games.Count, Is.EqualTo(data.Games.Values.Count(g => g.RawPbn == "Match0")));
      Assert.That(games, Has.All.Matches<Game>(g => g.RawPbn == "Match0"));
      games = sut.GetGames(gameEvent.Single(e => e.Name == "Event2")).ToList();
      Assert.That(games.Count, Is.EqualTo(data.Games.Values.Count(g => g.RawPbn == "Match1")));
      Assert.That(games, Has.All.Matches<Game>(g => g.RawPbn == "Match1"));
    }

    [Test]
    public void ShouldBeAbleToHandleNonAsciiCharactersWhenStoringEvents()
    {
      var sut = new Database(dialect_, connectionString_);
      sut.CreateSchema();

      var data = new Root();
      data.Events.Add(0, new Event { Id = 0, Name = "åäöéèü€Юԇێظרּﯤ", Site = "åäöéèü€Юԇێظרּﯤ", StartDate = new DateTime(1999, 12, 24), EndDate = new DateTime(1999, 12, 26) });
      sut.Persist(data);

      var events = sut.GetAllEvents().ToList();
      Assert.That(events.Single().Name, Is.EqualTo(data.Events[0].Name));
      Assert.That(events.Single().Site, Is.EqualTo(data.Events[0].Site));
    }

    [Test]
    public void ShouldFilterOutAllEventsWhereAtLeastOneGameIsPlayedByAnySpecifiedPlayerWhenGettingEventIdsForPlayers()
    {
      var sut = new Database(dialect_, connectionString_);
      sut.CreateSchema();

      var data = new Root();
      data.Players.Add(0, new Player { Id = 0, Name = "East" });
      data.Players.Add(1, new Player { Id = 1, Name = "West" });
      data.Players.Add(2, new Player { Id = 2, Name = "North" });
      data.Players.Add(3, new Player { Id = 3, Name = "South" });
      data.Players.Add(4, new Player { Id = 4, Name = "Extra" });
      data.Players.Add(5, new Player { Id = 5, Name = "Extra2" });
      data.Events.Add(0, new Event { Id = 0, Name = "Event name", Site = "Here" });
      data.Events.Add(1, new Event { Id = 1, Name = "Event2", Site = "Here" });
      data.Events.Add(2, new Event { Id = 2, Name = "Event3", Site = "Here" });
      data.Events.Add(3, new Event { Id = 3, Name = "Event4", Site = "Here" });
      data.Events.Add(4, new Event { Id = 4, Name = "Event5", Site = "Here" });
      data.Events.Add(5, new Event { Id = 5, Name = "Event6", Site = "There" });
      data.Games.Add(0, new Game { Id = 0, Contract = new Contract(), RawPbn = "Match0", EastId = 0, WestId = 1, NorthId = 2, SouthId = 3, EventId = 0 });
      data.Games.Add(1, new Game { Id = 1, Contract = new Contract(), RawPbn = "Match1", EastId = 4, WestId = 1, NorthId = 2, SouthId = 3, EventId = 1 });
      data.Games.Add(2, new Game { Id = 2, Contract = new Contract(), RawPbn = "Match0", EastId = 0, WestId = 4, NorthId = 2, SouthId = 3, EventId = 2 });
      data.Games.Add(3, new Game { Id = 3, Contract = new Contract(), RawPbn = "Match1", EastId = 5, WestId = 5, NorthId = 4, SouthId = 3, EventId = 3 });
      data.Games.Add(4, new Game { Id = 4, Contract = new Contract(), RawPbn = "Match0", EastId = 0, WestId = 1, NorthId = 2, SouthId = 4, EventId = 4 });
      data.Games.Add(5, new Game { Id = 5, Contract = new Contract(), RawPbn = "Match1", EastId = 0, WestId = 5, NorthId = 2, SouthId = 3, EventId = 5 });
      sut.Persist(data);

      var events = sut.GetEventIdsForPlayers(new[] { 4, 5 }).ToList();

      Assert.That(events, Is.EquivalentTo(new[] { 1, 2, 3, 4, 5 }));
    }
  }
}