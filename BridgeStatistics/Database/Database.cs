﻿namespace BridgeStatistics.Database
{
  using System;
  using System.Collections.Generic;
  using System.Data.Common;
  using System.Data.SqlClient;
  using System.Data.SQLite;
  using System.Globalization;
  using System.IO;
  using System.Linq;
  using System.Text;
  using System.Transactions;

  using Model;
  using Model.WriteModel;

  using NUnit.Framework;

#pragma warning disable CA1724
  public class Database : IDatabase
#pragma warning restore CA1724
  {
    private readonly Dialect dialect_;
    private readonly string connectionString_;

    private DbConnection cachedConnection_;

    public Database()
      : this(Dialect.Sqlite, $"Data Source={Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData)}\\BridgeStatistics\\database.db3;Version=3;")
    {
      if (!Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\BridgeStatistics"))
      {
        Directory.CreateDirectory(
          Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\BridgeStatistics");
      }

      if (!File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\BridgeStatistics\\database.db3"))
        CreateSchema();
    }

    internal Database(Dialect dialect, string connectionString)
    {
      dialect_ = dialect;
      connectionString_ = connectionString;
    }

    public void Persist(Root data)
    {
      var playerIdMap = new Dictionary<int, int>();
      var eventIdMap = new Dictionary<int, (int, bool)>();
      using (var transaction = new TransactionScope())
      {
        using (DbConnection connection = Connect_())
        {
          foreach (var player in data.Players.Values)
          {
            playerIdMap.Add(player.Id, PersistPlayer_(connection, player));
          }

          foreach (var @event in data.Events.Values)
          {
            eventIdMap.Add(@event.Id, PersistEvent_(connection, @event));
          }

          foreach (var game in data.Games.Values)
          {
            PersistGame_(connection, game, playerIdMap, eventIdMap);
          }

          transaction.Complete();
        }
      }
    }

    public IEnumerable<Player> GetAllPlayers()
    {
      using (DbConnection connection = Connect_())
      {
        const string sql = "Select Id, Name FROM Players";
        using (var command = connection.CreateCommand())
        {
          command.CommandText = sql;
          using (var reader = command.ExecuteReader())
          {
            while (reader.Read())
            {
              yield return new Player { Id = (int)reader["Id"], Name = (string)reader["Name"] };
            }
          }
        }
      }
    }

    public IEnumerable<Event> GetAllEvents()
    {
      using (DbConnection connection = Connect_())
      {
        const string sql = "Select Id, Name, Site, StartDate, EndDate FROM Events";
        using (var command = connection.CreateCommand())
        {
          command.CommandText = sql;
          using (var reader = command.ExecuteReader())
          {
            while (reader.Read())
            {
              yield return new Event { Id = (int)reader["Id"], Name = reader["Name"] as string, Site = reader["Site"] as string, StartDate = (DateTime)reader["StartDate"], EndDate = (DateTime)reader["EndDate"] };
            }
          }
        }
      }
    }

#pragma warning disable S3242 // Method parameters should be declared with base types
    public IEnumerable<Game> GetGames(Event @event)
#pragma warning restore S3242 // Method parameters should be declared with base types
    {
      using (DbConnection connection = Connect_())
      {
        string sql = "Select * FROM Games, Contracts WHERE ContractId = Contracts.Id AND EventId = @eventId";
        var parameterNames = new[] { "eventId" };
        var parameterValues = new object[] { @event.Id };

        using (var command = CreateCommandWithParameter_(connection, sql, parameterNames, parameterValues))
        {
          using (var reader = command.ExecuteReader())
          {
            while (reader.Read())
            {
              var contract = new Contract((int)reader["Tricks"], (Suit)Enum.Parse(typeof(Suit), (string)reader["Suit"]), (Risk)Enum.Parse(typeof(Risk), (string)reader["Risk"]));
              yield return new Game { Id = (int)reader["Id"], Board = (int)reader["Board"], Contract = contract, Date = (DateTime)reader["Date"], Deal = reader["Deal"] as string, Declarer = (PlayerDirection)Enum.Parse(typeof(PlayerDirection), (string)reader["Declarer"]), EastId = (int)reader["EastId"], WestId = (int)reader["WestId"], NorthId = (int)reader["NorthId"], SouthId = (int)reader["SouthId"], EventId = (int)reader["EventId"], Vulnerable = (Vulnerability)Enum.Parse(typeof(Vulnerability), (string)reader["Vulnerable"]), RawPbn = reader["RawPbn"] as string, Result = (int)reader["Result"], Score = new Score((int)reader["Score"]), ScoreIMP = (float?)(reader["ScoreIMP"] as double?), ScoreMP = (float?)(reader["ScoreMP"] as double?), ScorePercentage = (float?)(reader["ScorePercentage"] as double?), Scoring = reader["Scoring"] as string };
            }
          }
        }
      }
    }

    public IEnumerable<int> GetEventIdsForPlayers(IEnumerable<int> playerIds)
    {
      using (DbConnection connection = Connect_())
      {
        string sql = "Select distinct e.Id FROM Events e INNER JOIN Games g ON e.Id = g.EventId INNER JOIN Players p ON p.Id = g.WestId OR p.Id = g.NorthId OR p.Id = g.SouthId OR p.Id = g.EastId WHERE p.Id IN (" +
          string.Join(", ", playerIds) + ")";
        using (var command = connection.CreateCommand())
        {
          command.CommandText = sql;
          using (var reader = command.ExecuteReader())
          {
            while (reader.Read())
            {
              yield return (int)reader["Id"];
            }
          }
        }
      }
    }

    internal void DropSchema()
    {
      using (var connection = Connect_())
      {
        using (var command = connection.CreateCommand())
        {
          command.CommandText = DatabaseCreationUtils.DropSchemaSql;
          command.ExecuteNonQuery();
        }
      }
    }

    internal void CreateSchema()
    {
      using (var connection = Connect_())
      {
        using (var command = connection.CreateCommand())
        {
          command.CommandText = DatabaseCreationUtils.CreateSchemaSql;
          command.ExecuteNonQuery();
        }
      }
    }

    private void PersistGame_(DbConnection connection, Game game, Dictionary<int, int> playerIdMap, Dictionary<int, (int, bool)> eventIdMap)
    {
      var existingId = FindExistingId_(connection, game, playerIdMap, eventIdMap);
      if (existingId.HasValue) return;

      var contractId = FindExistingContractId_(connection, game);
      if (contractId == null)
      {
        contractId = InsertContract_(connection, game);
      }

      var id = FindNextId_(connection);
      InsertGameData_(connection, game, playerIdMap, eventIdMap, id, contractId);
    }

    private void InsertGameData_(
      DbConnection connection,
      Game game,
      Dictionary<int, int> playerIdMap,
      Dictionary<int, (int, bool)> eventIdMap,
      int id,
      int? contractId)
    {
      const string insertSql =
        "INSERT INTO Games VALUES (@id, @date, @board, @vulnerable, @deal, @scoring, @declarer, @result, @score, @scoreImp, @scoreMp, @scorePercentage, @rawPbn, @contractId, @westId, @northId, @eastId, @southId, @eventId)";
      ExecuteNonQuery_(
        connection,
        insertSql,
        new[]
          {
            "id", "date", "board", "vulnerable", "deal", "scoring", "declarer", "result", "score", "scoreImp",
            "scoreMp", "scorePercentage", "rawPbn", "contractId", "westId", "northId", "eastId", "southId", "eventId"
          },
        new object[]
          {
            id, game.Date, game.Board, game.Vulnerable.ToString(), game.Deal, game.Scoring, game.Declarer.ToString(),
            game.Result, game.Score.GetScore(game.Declarer), game.ScoreIMP, game.ScoreMP, game.ScorePercentage, game.RawPbn,
            contractId, playerIdMap[game.WestId], playerIdMap[game.NorthId], playerIdMap[game.EastId],
            playerIdMap[game.SouthId], eventIdMap[game.EventId].Item1
          });
    }

    private int FindNextId_(DbConnection connection)
    {
      const string findNextIdSql = "SELECT Coalesce(MAX(Id) + 1, 0) FROM Games";
      var id = ExecuteScalar_<int>(connection, findNextIdSql);
      return id;
    }

    private int? InsertContract_(DbConnection connection, Game game)
    {
      const string findNextContractIdSql = "SELECT Coalesce(MAX(Id) + 1, 0) FROM Contracts";
      const string insertContractSql = "INSERT INTO Contracts VALUES (@id, @tricks, @suit, @risk)";
      int? contractId = ExecuteScalar_<int>(connection, findNextContractIdSql);
      ExecuteNonQuery_(
        connection,
        insertContractSql,
        new[] { "id", "tricks", "suit", "risk" },
        new object[] { contractId, game.Contract.Tricks, game.Contract.Suit.ToString(), game.Contract.Risk.ToString() });
      return contractId;
    }

    private int? FindExistingContractId_(DbConnection connection, Game game)
    {
      const string findExistingContractSql =
        "SELECT Id FROM Contracts WHERE Tricks = @tricks AND Suit = @suit AND Risk = @risk";
      int? contractId = ExecuteScalar_<int?>(
        connection,
        findExistingContractSql,
        new[] { "tricks", "suit", "risk" },
        new object[] { game.Contract.Tricks, game.Contract.Suit.ToString(), game.Contract.Risk.ToString() });
      return contractId;
    }

    private int? FindExistingId_(
      DbConnection connection,
      Game game,
      Dictionary<int, int> playerIdMap,
      Dictionary<int, (int, bool)> eventIdMap)
    {
      const string findExistingSql =
        "SELECT Id FROM Games WHERE EventId = @eventId AND Board = @board AND Date = @date AND WestId = @westId AND NorthId = @northId AND EastId = @eastId AND SouthId = @southId";

      if (!eventIdMap[game.EventId].Item2) return null;

      int? existingId = ExecuteScalar_<int?>(
        connection,
        findExistingSql,
        new[] { "eventId", "board", "date", "westId", "northId", "eastId", "southId" },
        new object[]
          {
            eventIdMap[game.EventId].Item1, game.Board, game.Date, playerIdMap[game.WestId], playerIdMap[game.NorthId],
            playerIdMap[game.EastId], playerIdMap[game.SouthId]
          });
      return existingId;
    }

    private (int, bool) PersistEvent_(DbConnection connection, Event @event)
    {
      const string insertSql = "INSERT INTO Events VALUES (@id, @name, @site, @startDate, @endDate)";
      const string findExistingSql =
        "SELECT Id FROM Events WHERE Name = @name AND Site = @site AND StartDate = @startDate AND EndDate = @endDate";
      const string findNextIdSql = "SELECT Coalesce(MAX(Id) + 1, 0) FROM Events";

      int? existingId = ExecuteScalar_<int?>(
        connection,
        findExistingSql,
        new[] { "name", "site", "startDate", "endDate" },
        new object[] { @event.Name, @event.Site, @event.StartDate, @event.EndDate });

      if (existingId.HasValue) return (existingId.Value, true);

      var id = ExecuteScalar_<int>(connection, findNextIdSql);
      ExecuteNonQuery_(
        connection,
        insertSql,
        new[] { "id", "name", "site", "startDate", "endDate" },
        new object[] { id, @event.Name, @event.Site, @event.StartDate, @event.EndDate });

      return (id, false);
    }

    private int PersistPlayer_(DbConnection connection, Player player)
    {
      var existingId = FindExistingPlayerId_(connection, player);
      if (existingId.HasValue) return existingId.Value;

      var id = FindNextPlayerId_(connection);
      InsertPlayerData_(connection, player, id);
      return id;
    }

    private void InsertPlayerData_(DbConnection connection, Player player, int id)
    {
      const string insertSql = "INSERT INTO Players VALUES (@id, @name)";
      ExecuteNonQuery_(connection, insertSql, new[] { "id", "name" }, new object[] { id, player.Name });
    }

    private int FindNextPlayerId_(DbConnection connection)
    {
      const string findNextIdSql = "SELECT Coalesce(MAX(Id) + 1, 0) FROM Players";
      var id = ExecuteScalar_<int>(connection, findNextIdSql);
      return id;
    }

    private int? FindExistingPlayerId_(DbConnection connection, Player player)
    {
      const string findExistingSql = "SELECT Id FROM Players WHERE Name = @name";
      int? existingId = ExecuteScalar_<int?>(connection, findExistingSql, "name", player.Name);
      return existingId;
    }

    private DbConnection Connect_()
    {
      switch (dialect_)
      {
        case Dialect.Sqlite:
          if (cachedConnection_ == null)
            cachedConnection_ = new SQLiteConnection(connectionString_).OpenAndReturn();
          return new NonDisposedDbConnection(cachedConnection_);
        case Dialect.SqlServer2015:
          var sqlConnection = new SqlConnection(connectionString_);
          sqlConnection.Open();
          return sqlConnection;
        default:
          throw new InvalidOperationException("Dialect must be SQLite or SqlServer2015");
      }
    }

    private DbCommand CreateCommandWithParameter_(
      DbConnection connection,
      string sql,
      string[] parameterNames,
      object[] parameterValues)
    {
      Assert.That(parameterNames.Length, Is.EqualTo(parameterValues.Length));
      var command = connection.CreateCommand();
      command.CommandText = sql;
      for (int i = 0; i < parameterNames.Length; i++)
      {
        var parameter = command.CreateParameter();
        parameter.ParameterName = parameterNames[i];
        parameter.Value = AdjustValue_(parameterValues[i]);
        command.Parameters.Add(parameter);
      }

      return command;
    }

    private object AdjustValue_(object value)
    {
      if (value is DateTime date && date == DateTime.MinValue)
        return new DateTime(1900, 01, 01);

      if (value is null) return DBNull.Value;

      return value;
    }

    private void ExecuteNonQuery_(
      DbConnection connection,
      string sql)
    {
      ExecuteNonQuery_(connection, sql, Array.Empty<string>(), Array.Empty<object>());
    }

    private void ExecuteNonQuery_(
      DbConnection connection,
      string sql,
      string parameterName,
      object parameterValue)
    {
      ExecuteNonQuery_(connection, sql, new[] { parameterName }, new[] { parameterValue });
    }

    private void ExecuteNonQuery_(
      DbConnection connection,
      string sql,
      string[] parameterNames,
      object[] parameterValues)
    {
      using (var command = CreateCommandWithParameter_(connection, sql, parameterNames, parameterValues))
        command.ExecuteNonQuery();
    }

    private T ExecuteScalar_<T>(
      DbConnection connection,
      string sql)
    {
      return ExecuteScalar_<T>(connection, sql, Array.Empty<string>(), Array.Empty<object>());
    }

    private T ExecuteScalar_<T>(
      DbConnection connection,
      string sql,
      string parameterName,
      object parameterValue)
    {
      return ExecuteScalar_<T>(connection, sql, new[] { parameterName }, new[] { parameterValue });
    }

    private T ExecuteScalar_<T>(
      DbConnection connection,
      string sql,
      string[] parameterNames,
      object[] parameterValues)
    {
      using (var command = CreateCommandWithParameter_(connection, sql, parameterNames, parameterValues))
      {
        var value = command.ExecuteScalar();
        if (value is T) return (T)value;
        if (value is null) return (T)value;
        return (T)Convert.ChangeType(value, typeof(T), CultureInfo.InvariantCulture);
      }
    }
  }
}
