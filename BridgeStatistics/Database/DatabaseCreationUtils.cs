namespace BridgeStatistics.Database
{
#pragma warning disable S105
  internal static class DatabaseCreationUtils
  {
    internal const string CreateSchemaSql = @"
CREATE TABLE [Contracts](
	[Id] [int] NOT NULL,
	[Tricks] [int] NOT NULL,
	[Suit] [varchar](50) NOT NULL,
	[Risk] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Contracts] PRIMARY KEY  
(
	[Id] ASC
)
);

CREATE TABLE [Events](
	[Id] [int] NOT NULL,
	[Name] [nvarchar](4000) NOT NULL,
	[Site] [nvarchar](4000) NULL,
	[StartDate] datetime NULL,
	[EndDate] datetime NULL,
 CONSTRAINT [PK_Events] PRIMARY KEY  
(
	[Id] ASC
)
);

CREATE TABLE [Games](
	[Id] [int] NOT NULL,
	[Date] [datetime] NULL,
	[Board] [int] NOT NULL,
	[Vulnerable] [varchar](50) NOT NULL,
	[Deal] [varchar](8000) NULL,
	[Scoring] [varchar](8000) NULL,
	[Declarer] [varchar](50) NOT NULL,
	[Result] [int] NOT NULL,
	[Score] [int] NOT NULL,
	[ScoreImp] [float] NULL,
	[ScoreMp] [float] NULL,
	[ScorePercentage] [float] NULL,
	[RawPbn] [Text] NOT NULL,
	[ContractId] [int] NULL,
	[WestId] [int] NOT NULL,
	[NorthId] [int] NOT NULL,
	[EastId] [int] NOT NULL,
	[SouthId] [int] NOT NULL,
	[EventId] [int] NOT NULL,
 CONSTRAINT [PK_Games] PRIMARY KEY  
(
	[Id] ASC
)
);

CREATE TABLE [Players](
	[Id] [int] NOT NULL,
	[Name] [nvarchar](4000) NOT NULL,
 CONSTRAINT [PK_Players] PRIMARY KEY 
(
	[Id] ASC
)
);

CREATE INDEX FindExistingIdIndex ON Games
(
  [Id] ASC,
	[Date] ASC,
	[Board] ASC,
	[WestId] ASC,
	[NorthId] ASC,
	[EastId] ASC,
	[SouthId] ASC,
	[EventId] ASC
);

CREATE INDEX [GetGamesIndex] ON [Games](
  [EventId] ASC, 
  [ContractId] ASC, 
  [Id] ASC
);
";
////ALTER TABLE [Games]  WITH CHECK ADD  CONSTRAINT [FK_Games_Contracts] FOREIGN KEY([ContractId])
////REFERENCES [Contracts] ([Id]);
////ALTER TABLE [Games] CHECK CONSTRAINT [FK_Games_Contracts];
////ALTER TABLE [Games]  WITH CHECK ADD  CONSTRAINT [FK_Games_Events] FOREIGN KEY([EventId])
////REFERENCES [Events] ([Id]);
////ALTER TABLE [Games] CHECK CONSTRAINT [FK_Games_Events];
////ALTER TABLE [Games]  WITH CHECK ADD  CONSTRAINT [FK_Games_Players] FOREIGN KEY([EastId])
////REFERENCES [Players] ([Id]);
////ALTER TABLE [Games] CHECK CONSTRAINT [FK_Games_Players];
////ALTER TABLE [Games]  WITH CHECK ADD  CONSTRAINT [FK_Games_Players1] FOREIGN KEY([SouthId])
////REFERENCES [Players] ([Id]);
////ALTER TABLE [Games] CHECK CONSTRAINT [FK_Games_Players1];
////ALTER TABLE [Games]  WITH CHECK ADD  CONSTRAINT [FK_Games_Players2] FOREIGN KEY([NorthId])
////REFERENCES [Players] ([Id]);
////ALTER TABLE [Games] CHECK CONSTRAINT [FK_Games_Players2];
////ALTER TABLE [Games]  WITH CHECK ADD  CONSTRAINT [FK_Games_Players3] FOREIGN KEY([WestId])
////REFERENCES [Players] ([Id]);
////ALTER TABLE [Games] CHECK CONSTRAINT [FK_Games_Players3];
////";

    internal const string DropSchemaSql = @"
IF OBJECT_ID('[Games]', 'U') IS NOT NULL 
  DROP TABLE [Games];
IF OBJECT_ID('[Players]', 'U') IS NOT NULL 
  DROP TABLE [Players];
IF OBJECT_ID('[Events]', 'U') IS NOT NULL 
  DROP TABLE [Events];
IF OBJECT_ID('[Contracts]', 'U') IS NOT NULL 
  DROP TABLE [Contracts];";
  }
}
#pragma warning restore S105
