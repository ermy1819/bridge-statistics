﻿namespace BridgeStatistics.ViewModels
{
  using BridgeStatistics.Model.ReadModel;

  public class DealFilterVM : ViewModel, IFilterData
  {
    public IContractFilterData Contract { get; } = new ContractFilterVM();

    public IResultFilterData Result { get; } = new ResultFilterVM();

    public IRoleFilterData Role { get; } = new RoleFilterVM();

    public IVulnerabilityFilterData Vulnerability { get; } = new VulnerabilityFilterVM();
 }
}
