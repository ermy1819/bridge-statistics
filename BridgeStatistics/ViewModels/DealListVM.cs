﻿namespace BridgeStatistics.ViewModels
{
  using System.Collections.Generic;
  using System.Collections.ObjectModel;
  using System.Configuration;
  using System.Linq;
  using Model.ReadModel;
  using Model.WriteModel;

  public class DealListVM : ViewModel
  {
    private readonly Root root_;
    private readonly ISelection selection_;

    private ObservableCollection<DealVM> items_;

    /// <summary>
    /// Initializes a new instance of the <see cref="DealListVM"/> class. 
    /// For Wpf designer only
    /// </summary>
    public DealListVM()
    {
      // For Wpf designer only
    }

    public DealListVM(Root root, ISelection selection)
    {
      root_ = root;
      selection_ = selection;
    }

    public Command Search => new Command(OnSearchClicked_);

    public ObservableCollection<DealVM> Items
    {
      get => items_;
      private set
      {
        if (value == items_) return;
        items_ = value;
        OnPropertyChanged_();
      }
    }

    public int NumberOfDeals => PlayerDeals_.Count;
    public int PercentageMade =>
      PlayerDeals_.Count(p => p.Result.RelativeTricks >= 0 && !p.Contract.IsPass) * 100 / NumberOfDeals;
    public int Imps => (int)PlayerDeals_.Sum(p => p.ScoreIMP);
    public int ImpPercentage => PlayerDeals_.Count(p => p.ScoreIMP > 0) * 100 / NumberOfDeals;
    public int MatchPoints => (int)PlayerDeals_.Average(p => p.ScoreMP);
    public int MatchPointPercentage => PlayerDeals_.Count(p => p.ScoreMP > 50) * 100 / NumberOfDeals;
    public int Games => PlayerDeals_.Count(p => p.Contract.IsGame);
    public int GamePercentage => PlayerDeals_.Count(p => p.Contract.IsGame && !p.Result.IsDefeated) * 100 / Games;
    public int SmallSlams => PlayerDeals_.Count(p => p.Contract.Tricks == 6);
    public int SmallSlamPercentage => PlayerDeals_.Count(p => p.Contract.Tricks == 6 && !p.Result.IsDefeated) * 100 / SmallSlams;
    public int GrandSlams => PlayerDeals_.Count(p => p.Contract.Tricks == 7);
    public int GrandSlamPercentage => PlayerDeals_.Count(p => p.Contract.Tricks == 7 && !p.Result.IsDefeated) * 100 / GrandSlams;
    public int Doubled => PlayerDeals_.Count(p => p.Contract.Risk.IsDoubled);
    public int DoubledPercentage => PlayerDeals_.Count(p => p.Contract.Risk.IsDoubled && !p.Result.IsDefeated) * 100 / Doubled;
    public int Redoubled => PlayerDeals_.Count(p => p.Contract.Risk.IsRedoubled);
    public int RedoubledPercentage => PlayerDeals_.Count(p => p.Contract.Risk.IsRedoubled && !p.Result.IsDefeated) * 100 / Redoubled;
    public int MostFrequentContract => Deals_.Sum(d => d.PlayerDeals.Count(p => p.Contract.Risk.TrickScoreMultiplier == d.MostFrequentContract.Risk.TrickScoreMultiplier && p.Contract.Suit.Suit == d.MostFrequentContract.Suit.Suit && p.Contract.Tricks == d.MostFrequentContract.Tricks && p.Contract.Declarer.IsNorthOrSouth == d.MostFrequentContract.Declarer.IsNorthOrSouth));
    public int MostFrequentContractPercentage => Deals_.Sum(d => d.PlayerDeals.Count(p => p.Contract.Risk.TrickScoreMultiplier == d.MostFrequentContract.Risk.TrickScoreMultiplier && p.Contract.Suit.Suit == d.MostFrequentContract.Suit.Suit && p.Contract.Tricks == d.MostFrequentContract.Tricks && p.Contract.Declarer.IsNorthOrSouth == d.MostFrequentContract.Declarer.IsNorthOrSouth && !p.Result.IsDefeated)) * 100 / MostFrequentContract;
    public int NoTrump => PlayerDeals_.Count(p => !p.Contract.IsPass && p.Contract.Suit.IsNoTrump);
    public int NoTrumpPercentage => PlayerDeals_.Count(p => !p.Contract.IsPass && p.Contract.Suit.IsNoTrump && !p.Result.IsDefeated) * 100 / NoTrump;
    public int Spades => PlayerDeals_.Count(p => p.Contract.Suit.Suit == Suit.Spades);
    public int SpadesPercentage => PlayerDeals_.Count(p => p.Contract.Suit.Suit == Suit.Spades && !p.Result.IsDefeated) * 100 / Spades;
    public int Hearts => PlayerDeals_.Count(p => p.Contract.Suit.Suit == Suit.Hearts);
    public int HeartsPercentage => PlayerDeals_.Count(p => p.Contract.Suit.Suit == Suit.Hearts && !p.Result.IsDefeated) * 100 / Hearts;
    public int Diamonds => PlayerDeals_.Count(p => p.Contract.Suit.Suit == Suit.Diamonds);
    public int DiamondsPercentage => PlayerDeals_.Count(p => p.Contract.Suit.Suit == Suit.Diamonds && !p.Result.IsDefeated) * 100 / Diamonds;
    public int Clubs => PlayerDeals_.Count(p => p.Contract.Suit.Suit == Suit.Clubs);
    public int ClubsPercentage => PlayerDeals_.Count(p => p.Contract.Suit.Suit == Suit.Clubs && !p.Result.IsDefeated) * 100 / Clubs;
    public int MostFrequentScore => Deals_.Sum(d => d.PlayerDeals.Count(p => p.Score * (p.Position.IsNorthOrSouth ? 1 : -1) == d.MostFrequentScore));
    public int MostFrequentScorePercentage => Deals_.Sum(d => d.PlayerDeals.Count(p => !p.Contract.IsPass && !p.Result.IsDefeated && p.Score * (p.Position.IsNorthOrSouth ? 1 : -1) == d.MostFrequentScore)) * 100 / MostFrequentScore;
    public int BeatMostFrequentScore => Deals_.Sum(d => d.PlayerDeals.Count(p => p.Score > (p.Position.IsNorthOrSouth ? 1 : -1) * d.MostFrequentScore));
    public int BeatMostFrequentScorePercentage => BeatMostFrequentScore * 100 / Deals_.Sum(d => d.PlayerDeals.Count(p => p.Score != (p.Position.IsNorthOrSouth ? 1 : -1) * d.MostFrequentScore));

    private IReadOnlyCollection<IDeal> Deals_ => selection_.Deals;

    private IList<IPlayerDeal> PlayerDeals_
    {
      get;
      set;
    }

    private void OnSearchClicked_()
    {
      root_.Games.Clear();
      root_.Games.Fetch(true);
      selection_.Repopulate();
      Items = new ObservableCollection<DealVM>(selection_.Deals.SelectMany(d => d.PlayerDeals.Select(p => new DealVM(d, p))).ToList());
      PlayerDeals_ = selection_.Deals.SelectMany(d => d.PlayerDeals).ToList();
      OnPropertyChanged_(null);
    }
  }
}