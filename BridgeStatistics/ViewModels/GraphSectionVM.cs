﻿namespace BridgeStatistics.ViewModels
{
  using System;
  using System.Globalization;
  using System.Linq;

  using BridgeStatistics.Model.ReadModel;

  using OxyPlot;
  using OxyPlot.Axes;
  using OxyPlot.Series;

  public class GraphSectionVM : ViewModel
  {
    private readonly ISelection selection_;

    public GraphSectionVM(ISelection selection)
    {
      selection_ = selection;
      selection_.Modified += UpdatePlotModels_;
    }

    public PlotModel PlotModel1 { get; private set; }

    public PlotModel PlotModel2 { get; private set; }

    public PlotModel PlotModel3 { get; private set; }

    public PlotModel PlotModel4 { get; private set; }

    public PlotModel PlotModel5 { get; private set; }

    private void UpdatePlotModels_(object sender, EventArgs e)
    {
      var playerDeals = selection_.Deals.SelectMany(d => d.PlayerDeals).ToList();
      PlotModel1 = new PlotModel
                     {
                       Title = "Matchpoint distribution",
                       Series =
                         {
                           new BarSeries
                             {
                               Items =
                                 {
                                   new BarItem(playerDeals.Count(p => p.ScoreMP < 10)),
                                   new BarItem(playerDeals.Count(p => p.ScoreMP < 20 && p.ScoreMP >= 10)),
                                   new BarItem(playerDeals.Count(p => p.ScoreMP < 30 && p.ScoreMP >= 20)),
                                   new BarItem(playerDeals.Count(p => p.ScoreMP < 40 && p.ScoreMP >= 30)),
                                   new BarItem(playerDeals.Count(p => p.ScoreMP < 50 && p.ScoreMP >= 40)),
                                   new BarItem(playerDeals.Count(p => p.ScoreMP < 60 && p.ScoreMP >= 50)),
                                   new BarItem(playerDeals.Count(p => p.ScoreMP < 70 && p.ScoreMP >= 60)),
                                   new BarItem(playerDeals.Count(p => p.ScoreMP < 80 && p.ScoreMP >= 70)),
                                   new BarItem(playerDeals.Count(p => p.ScoreMP < 90 && p.ScoreMP >= 80)),
                                   new BarItem(playerDeals.Count(p => p.ScoreMP >= 90))
                                 }
                             }
                         },
                       Axes =
                         {
                           new LinearAxis { Position = AxisPosition.Bottom },
                           new CategoryAxis
                             {
                               Position = AxisPosition.Left,
                               Labels = { "[0, 10)", "[10, 20)", "[20, 30)", "[30, 40)", "[40, 50)", "[50, 60)", "[60, 70)", "[70, 80)", "[80, 90)", "[90, 100]" }
                             }
                         }
                     };

      PlotModel2 = new PlotModel
                     {
                       Title = "Cross Imp distribution",
                       Series =
                         {
                           new BarSeries
                             {
                               Items =
                                 {
                                   new BarItem(playerDeals.Count(p => p.ScoreIMP < -9)),
                                   new BarItem(playerDeals.Count(p => p.ScoreIMP < -7 && p.ScoreIMP >= -9)),
                                   new BarItem(playerDeals.Count(p => p.ScoreIMP < -5 && p.ScoreIMP >= -7)),
                                   new BarItem(playerDeals.Count(p => p.ScoreIMP < -3 && p.ScoreIMP >= -5)),
                                   new BarItem(playerDeals.Count(p => p.ScoreIMP < -1 && p.ScoreIMP >= -3)),
                                   new BarItem(playerDeals.Count(p => p.ScoreIMP <= 1 && p.ScoreIMP >= -1)),
                                   new BarItem(playerDeals.Count(p => p.ScoreIMP <= 3 && p.ScoreIMP > 1)),
                                   new BarItem(playerDeals.Count(p => p.ScoreIMP <= 5 && p.ScoreIMP > 3)),
                                   new BarItem(playerDeals.Count(p => p.ScoreIMP <= 7 && p.ScoreIMP > 5)),
                                   new BarItem(playerDeals.Count(p => p.ScoreIMP <= 9 && p.ScoreIMP > 7)),
                                   new BarItem(playerDeals.Count(p => p.ScoreIMP > 9))
                                 }
                             }
                         },
                       Axes =
                         {
                           new LinearAxis { Position = AxisPosition.Bottom },
                           new CategoryAxis
                             {
                               Position = AxisPosition.Left,
                               Labels = { "<-9", "[-9, -7)", "[-7, -5)", "[-5, -3)", "[-3, -1)", "[-1, 1]", "(1, 3]", "(3, 5]", "(5, 7]", "(7, 9]", ">9" }
                             }
                         }
                     };

      PlotModel3 = new PlotModel
                     {
                       Title = "Results playing",
                       Series =
                         {
                           new PieSeries
                             {
                               Slices = playerDeals.Where(p => !p.Contract.IsPass && !p.Role.IsDefending).GroupBy(p => p.Result.RelativeTricks).OrderBy(g => g.Key).Select(g => new PieSlice(g.Key.ToString(CultureInfo.InvariantCulture), g.Count())).ToList()
                             }
                         }
                     };

      PlotModel4 = new PlotModel
                     {
                       Title = "Results defending",
                       Series =
                         {
                           new PieSeries
                             {
                               Slices = playerDeals.Where(p => !p.Contract.IsPass && p.Role.IsDefending).GroupBy(p => p.Result.RelativeTricks).OrderBy(g => g.Key).Select(g => new PieSlice(g.Key.ToString(CultureInfo.InvariantCulture), g.Count())).ToList()
                             }
                         }
                     };

      PlotModel5 = new PlotModel
                     {
                       Title = "Play type",
                       Series =
                         {
                           new PieSeries
                             {
                               Slices =
                                 {
                                   new PieSlice("Slam defending", playerDeals.Count(p => !p.Contract.IsPass && p.Contract.Tricks > 5 && p.Role.IsDefending)),
                                   new PieSlice("Game defending", playerDeals.Count(p => !p.Contract.IsPass && p.Contract.IsGame && p.Contract.Tricks < 6 && p.Role.IsDefending)),
                                   new PieSlice("Part score defending", playerDeals.Count(p => !p.Contract.IsPass && !p.Contract.IsGame && p.Role.IsDefending)),
                                   new PieSlice("Pass", playerDeals.Count(p => p.Contract.IsPass)),
                                   new PieSlice("Part score playing", playerDeals.Count(p => !p.Contract.IsPass && !p.Contract.IsGame && !p.Role.IsDefending)),
                                   new PieSlice("Game playing", playerDeals.Count(p => !p.Contract.IsPass && p.Contract.IsGame && p.Contract.Tricks < 6 && !p.Role.IsDefending)),
                                   new PieSlice("Slam playing", playerDeals.Count(p => !p.Contract.IsPass && p.Contract.Tricks > 5 && !p.Role.IsDefending)),
                                 }
                             }
                         }
                     };

      OnPropertyChanged_(null);
    }
  }
}