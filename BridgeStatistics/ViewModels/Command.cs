﻿namespace BridgeStatistics.ViewModels
{
  using System;
  using System.Windows.Input;

  public class Command : ICommand
  {
    private readonly Action<object> execute_;
    private readonly Func<object, bool> canExecute_;

    public Command()
    {      
    }

    public Command(Action<object> execute, Func<object, bool> canExecute = null)
    {
      execute_ = execute;
      canExecute_ = canExecute;
    }

    public Command(Action execute, Func<bool> canExecute = null)
    {
      if (execute != null)
        execute_ = _ => execute();
      if (canExecute_ != null)
        canExecute_ = _ => canExecute();
    }

    public event EventHandler CanExecuteChanged;

    public virtual bool CanExecute(object parameter)
    {
      return canExecute_?.Invoke(parameter) ?? true;
    }

    public virtual void Execute(object parameter)
    {
      if (execute_ != null)
        execute_(parameter);
      else
        throw new NotImplementedException();
    }

#pragma warning disable CA1030 // Use events where appropriate
    public void RaiseCanExecuteChanged()
#pragma warning restore CA1030 // Use events where appropriate
    {
      CanExecuteChanged?.Invoke(this, new EventArgs());
    }
  }
}