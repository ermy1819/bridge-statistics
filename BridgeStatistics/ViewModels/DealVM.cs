﻿namespace BridgeStatistics.ViewModels
{
  using Model.ReadModel;

  public class DealVM : ViewModel
  {
    public DealVM(IDeal deal, IPlayerDeal stats)
    {
      Deal = deal;
      Stats = stats;
    }

    public IDeal Deal { get; set; }
    public IPlayerDeal Stats { get; set; }

    public int MostFrequentScore => Deal.MostFrequentScore * (Stats.Position.IsNorthOrSouth ? 1 : -1);
  }
}