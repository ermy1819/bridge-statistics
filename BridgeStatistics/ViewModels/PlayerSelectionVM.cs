﻿namespace BridgeStatistics.ViewModels
{
  using System;
  using System.IO;
  using System.Linq;

  using Microsoft.Win32;

  using Model.WriteModel;

  public class PlayerSelectionVM : ViewModel
  {
    private readonly Root root_;

    public PlayerSelectionVM()
    {
      ImportPbn = new Command(OnImportPbnExecuted_);
      SelectEvent = new Command(OnSelectEventExecuted_);
      SelectPlayer = new Command(OnSelectPlayerExecuted_);
    }

    public PlayerSelectionVM(Root root) 
      : this()
    {
      root_ = root;
    }

    public Command ImportPbn { get; }
    public Command SelectEvent { get; }
    public Command SelectPlayer { get; }

    public string SelectedEvents
    {
      get
      {
        return string.Join(
          Environment.NewLine,
          root_.Events.Values.Select(e => e.DisplayName));
      }
    }

    public string SelectedPlayers
    {
      get
      {
        return string.Join(
          Environment.NewLine,
          root_.Players.Values.Select(p => p.Name));
      }
    }

    private void OnImportPbnExecuted_()
    {
      var dialog = new OpenFileDialog
      {
        CheckFileExists = true,
        Filter = "Portable Bridge Notation|*.pbn|All Files|*.*",
        Multiselect = true,
      };

      var result = dialog.ShowDialog();
      if (result == true)
      {
        foreach (var file in dialog.OpenFiles())
        {
          var data = new PbnParser.PbnParser().Parse(new StreamReader(file));
          root_.Persist(data);
        }
      }
    }

    private void OnSelectPlayerExecuted_()
    {
      var selectionVm = new SelectPlayerVm(root_.Players.Fetch(false));
      var result = selectionVm.ShowDialog();
      if (result != true) return;

      root_.Players.Clear();
      foreach (var player in selectionVm.SelectedItems)
      {
        root_.Players.Add(player.Id, player);
      }

      OnPropertyChanged_(nameof(SelectedPlayers));
    }

    private void OnSelectEventExecuted_()
    {
      var eventsOfInterest = root_.FetchEventIdsForCurrentPlayers().ToList();
      var selectionVm = new SelectEventVm(root_.Events.Fetch(false).Where(e => eventsOfInterest.Contains(e.Id)).ToList());
      var result = selectionVm.ShowDialog();
      if (result != true) return;

      root_.Events.Clear();
      foreach (var @event in selectionVm.SelectedItems)
      {
        root_.Events.Add(@event.Id, @event);
      }

      OnPropertyChanged_(nameof(SelectedEvents));
    }
  }
}