﻿namespace BridgeStatistics.ViewModels
{
  using BridgeStatistics.Model.ReadModel;
  using BridgeStatistics.Model.WriteModel;

  public class WorkspaceVM : ViewModel
  {
    /// <summary>
    /// Initializes a new instance of the <see cref="WorkspaceVM"/> class. 
    /// For WPF designer only
    /// </summary>
    public WorkspaceVM()
    {      
    }

    public WorkspaceVM(Root root, Filter filter, ISelection selection)
    {
      PlayerSelection = new PlayerSelectionVM(root);
      DealFilter = new DealFilterVM();
      filter.SetData(DealFilter);
      DealList = new DealListVM(root, selection);
      GraphSection = new GraphSectionVM(selection);
    }

    public PlayerSelectionVM PlayerSelection { get; }
    public DealFilterVM DealFilter { get; }
    public DealListVM DealList { get; }
    public GraphSectionVM GraphSection { get; }
  }
}
