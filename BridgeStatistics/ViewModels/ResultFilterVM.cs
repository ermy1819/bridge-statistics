﻿namespace BridgeStatistics.ViewModels
{
  using System.Globalization;
  using System.Runtime.CompilerServices;

  using BridgeStatistics.Model.ReadModel;

  public class ResultFilterVM : ViewModel, IResultFilterData
  {
    private int? playerAbsoluteMin_;
    private int? playerAbsoluteMax_;
    private int? playerRelativeMin_;
    private int? playerRelativeMax_;
    private int? declarerAbsoluteMin_;
    private int? declarerAbsoluteMax_;
    private int? declarerRelativeMin_;
    private int? declarerRelativeMax_;

    public int? PlayerAbsoluteMin { get => playerAbsoluteMin_; set => SetResultValue_(ref playerAbsoluteMin_, value); }
    public int? PlayerAbsoluteMax { get => playerAbsoluteMax_; set => SetResultValue_(ref playerAbsoluteMax_, value); }
    public int? PlayerRelativeMin { get => playerRelativeMin_; set => SetResultValue_(ref playerRelativeMin_, value); }
    public int? PlayerRelativeMax { get => playerRelativeMax_; set => SetResultValue_(ref playerRelativeMax_, value); }
    public int? DeclarerAbsoluteMin { get => declarerAbsoluteMin_; set => SetResultValue_(ref declarerAbsoluteMin_, value); }
    public int? DeclarerAbsoluteMax { get => declarerAbsoluteMax_; set => SetResultValue_(ref declarerAbsoluteMax_, value); }
    public int? DeclarerRelativeMin { get => declarerRelativeMin_; set => SetResultValue_(ref declarerRelativeMin_, value); }
    public int? DeclarerRelativeMax { get => declarerRelativeMax_; set => SetResultValue_(ref declarerRelativeMax_, value); }

    public string Header
    {
      get
      {
        string result = null;
        if (PlayerAbsoluteMin.HasValue || PlayerAbsoluteMax.HasValue)
        {
          if (!PlayerAbsoluteMin.HasValue) result = "Won tricks: At most " + PlayerAbsoluteMax + " ";
          else if (!PlayerAbsoluteMax.HasValue) result = "Won tricks: At least " + PlayerAbsoluteMin + " ";
          else result = $"Won tricks: {PlayerAbsoluteMin} - {PlayerAbsoluteMax} ";
        }

        if (PlayerRelativeMin.HasValue || PlayerRelativeMax.HasValue)
        {
          if (!PlayerRelativeMin.HasValue) result += "Result: At best " + PlayerRelativeMax.Value.ToString("+#;-#;=", CultureInfo.InvariantCulture) + " ";
          else if (!PlayerRelativeMax.HasValue) result += "Result: At worst " + PlayerRelativeMin.Value.ToString("+#;-#;=", CultureInfo.InvariantCulture) + " ";
          else result += $"Result: {PlayerRelativeMin.Value.ToString("+#;-#;=", CultureInfo.InvariantCulture)} - {PlayerRelativeMax.Value.ToString("+#;-#;=", CultureInfo.InvariantCulture)} ";
        }

        if (DeclarerAbsoluteMin.HasValue || DeclarerAbsoluteMax.HasValue)
        {
          if (!DeclarerAbsoluteMin.HasValue) result += "Won tricks: At most " + DeclarerAbsoluteMax + " ";
          else if (!DeclarerAbsoluteMax.HasValue) result += "Won tricks: At least " + DeclarerAbsoluteMin + " ";
          else result += $"Won tricks: {DeclarerAbsoluteMin} - {DeclarerAbsoluteMax} ";
        }

        if (DeclarerRelativeMin.HasValue || DeclarerRelativeMax.HasValue)
        {
          if (!DeclarerRelativeMin.HasValue) result += "Result: At best " + DeclarerRelativeMax.Value.ToString("+#;-#;=", CultureInfo.InvariantCulture);
          else if (!DeclarerRelativeMax.HasValue) result += "Result: At worst " + DeclarerRelativeMin.Value.ToString("+#;-#;=", CultureInfo.InvariantCulture);
          else result += $"Result: {DeclarerRelativeMin.Value.ToString("+#;-#;=", CultureInfo.InvariantCulture)} - {DeclarerRelativeMax.Value.ToString("+#;-#;=", CultureInfo.InvariantCulture)}";
        }

        return result ?? "Won tricks: Any";
      }
    }

    private void SetResultValue_(ref int? field, int? value, [CallerMemberName] string property = null)
    {
      SetValue_(ref field, value, property, nameof(Header));
    }
  }
}