﻿namespace BridgeStatistics.ViewModels
{
  using System.Collections.Generic;
  using System.Globalization;
  using System.Linq;
  using System.Runtime.CompilerServices;
  using System.Text;

  using BridgeStatistics.Model.ReadModel;
  public class ContractFilterVM : ViewModel, IContractFilterData
  {
    private bool noTrump_;
    private bool spades_;
    private bool hearts_;
    private bool diamonds_;
    private bool clubs_;
    private bool pass_;
    private bool tricks0_;
    private bool tricks1_;
    private bool tricks2_;
    private bool tricks3_;
    private bool tricks4_;
    private bool tricks5_;
    private bool tricks6_;
    private bool tricks7_;
    private bool game_;
    private bool undoubled_;
    private bool doubled_;
    private bool redoubled_;

    public bool NoTrump { get => noTrump_; set => SetContractValue_(ref noTrump_, value); }
    public bool Spades { get => spades_; set => SetContractValue_(ref spades_, value); }
    public bool Hearts { get => hearts_; set => SetContractValue_(ref hearts_, value); }
    public bool Diamonds { get => diamonds_; set => SetContractValue_(ref diamonds_, value); }
    public bool Clubs { get => clubs_; set => SetContractValue_(ref clubs_, value); }
    public bool Pass { get => pass_; set => SetContractValue_(ref pass_, value); }
    public bool Tricks0 { get => tricks0_; set => SetContractValue_(ref tricks0_, value); }
    public bool Tricks1 { get => tricks1_; set => SetContractValue_(ref tricks1_, value); }
    public bool Tricks2 { get => tricks2_; set => SetContractValue_(ref tricks2_, value); }
    public bool Tricks3 { get => tricks3_; set => SetContractValue_(ref tricks3_, value); }
    public bool Tricks4 { get => tricks4_; set => SetContractValue_(ref tricks4_, value); }
    public bool Tricks5 { get => tricks5_; set => SetContractValue_(ref tricks5_, value); }
    public bool Tricks6 { get => tricks6_; set => SetContractValue_(ref tricks6_, value); }
    public bool Tricks7 { get => tricks7_; set => SetContractValue_(ref tricks7_, value); }
    public bool Game { get => game_; set => SetContractValue_(ref game_, value); }
    public bool Undoubled { get => undoubled_; set => SetContractValue_(ref undoubled_, value); }
    public bool Doubled { get => doubled_; set => SetContractValue_(ref doubled_, value); }
    public bool Redoubled { get => redoubled_; set => SetContractValue_(ref redoubled_, value); }

    public string Header
    {
      get
      {
        var suits = new List<bool>
                      {
                        NoTrump,
                        Spades,
                        Hearts,
                        Diamonds,
                        Clubs,
                        Pass
                      };
        var tricks = new List<bool>
                       {
                         Tricks0,
                         Tricks1,
                         Tricks2,
                         Tricks3,
                         Tricks4,
                         Tricks5,
                         Tricks6,
                         Tricks7,
                         Game
                       };
        var risks = new List<bool> { Undoubled, Doubled, Redoubled };

        if (!suits.Any(s => s) && !tricks.Any(t => t) && !risks.Any(r => r)) return "Contracts: All";
        if (suits.All(s => s) && tricks.All(t => t) && risks.All(r => r)) return "Contracts: All";

        StringBuilder result = new StringBuilder("Contracts: ");
        if (suits.Any(s => s) && !suits.All(s => s))
        {
          var strings = new List<string>();
          if (NoTrump) strings.Add("NT");
          if (Spades) strings.Add("♠");
          if (Hearts) strings.Add("♥");
          if (Diamonds) strings.Add("♦");
          if (Clubs) strings.Add("♣");
          if (Pass) strings.Add("p");
          result.Append($"{{{string.Join(",", strings)}}}");
        }

        if (tricks.Any(s => s) && !tricks.All(s => s) && !(tricks.Count(t => t) == 8 && !Game))
        {
          var strings = tricks.Take(8).Select((t, i) => (t, i)).Where(t => t.Item1).Select(t => t.Item2.ToString(CultureInfo.InvariantCulture)).ToList();
          if (Game && !(Tricks3 && Tricks4 && Tricks5)) strings.Add("Game");

          result.Append($"{{{string.Join(",", strings)}}}");
        }

        if (risks.Any(s => s) && !risks.All(s => s))
        {
          var strings = new List<string>();
          if (Undoubled) strings.Add("-");
          if (Doubled) strings.Add("X");
          if (Redoubled) strings.Add("XX");

          result.Append($"{{{string.Join(",", strings)}}}");
        }

        return result.ToString();
      }
    }

    private void SetContractValue_(ref bool field, bool value, [CallerMemberName] string property = null)
    {
      SetValue_(ref field, value, property, nameof(Header));
    }
  }
}