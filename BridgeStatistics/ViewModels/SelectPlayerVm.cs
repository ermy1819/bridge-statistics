﻿namespace BridgeStatistics.ViewModels
{
  using System.Collections.Generic;
  using System.Linq;
  using System.Text.RegularExpressions;

  using BridgeStatistics.Model.WriteModel;

  public class SelectPlayerVm : DialogViewModel
  {
    private readonly List<Player> allPlayers_;
    private List<Player> filteredPlayers_;
    private string filter_ = string.Empty;

    public SelectPlayerVm(List<Player> players)
    {
      allPlayers_ = players;
      filteredPlayers_ = new List<Player>(allPlayers_);
    }

    public override string Title => "Select player(s)";

    public string Filter
    {
      get => filter_;
      set
      {
        if (value == filter_) return;
        filter_ = value;
        Players = allPlayers_.Where(p => Regex.IsMatch(p.Name, Filter)).ToList();
        OnPropertyChanged_(nameof(Filter));
      }
    }

    public List<Player> Players
    {
      get => filteredPlayers_;
      private set
      {
        if (value == filteredPlayers_) return;
        filteredPlayers_ = value;
        OnPropertyChanged_(nameof(Players));
      }
    }

    public List<Player> SelectedItems { get; private set; }

    public Command OkCommand => new Command(OnOkClicked_);
    public Command OkAllCommand => new Command(OnOkAllClicked_);
    public Command CancelCommand => new Command(() => Close(false));

    private void OnOkClicked_(object parameter)
    {
      SelectedItems = ((IEnumerable<object>)parameter).Cast<Player>().ToList();
      Close(true);
    }

    private void OnOkAllClicked_()
    {
      SelectedItems = Players;
      Close(true);
    }
  }
}