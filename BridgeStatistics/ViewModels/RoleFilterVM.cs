﻿namespace BridgeStatistics.ViewModels
{
  using System.Collections.Generic;
  using System.Linq;
  using System.Runtime.CompilerServices;

  using BridgeStatistics.Model.ReadModel;

  public class RoleFilterVM : ViewModel, IRoleFilterData
  {
    private bool declarer_;
    private bool lhd_;
    private bool dummy_;
    private bool rhd_;
    private bool dealer_;
    private bool second_;
    private bool third_;
    private bool fourth_;

    public bool Declarer { get => declarer_; set => SetRoleValue_(ref declarer_, value); }
    public bool Lhd { get => lhd_; set => SetRoleValue_(ref lhd_, value); }
    public bool Dummy { get => dummy_; set => SetRoleValue_(ref dummy_, value); }
    public bool Rhd { get => rhd_; set => SetRoleValue_(ref rhd_, value); }
    public bool Dealer { get => dealer_; set => SetRoleValue_(ref dealer_, value); }
    public bool Second { get => second_; set => SetRoleValue_(ref second_, value); }
    public bool Third { get => third_; set => SetRoleValue_(ref third_, value); }
    public bool Fourth { get => fourth_; set => SetRoleValue_(ref fourth_, value); }

    public string Header
    {
      get
      {
        var roles = new List<string>();
        if (Declarer) roles.Add("declarer");
        if (Dummy) roles.Add("dummy");
        if (Lhd) roles.Add("LHD");
        if (Rhd) roles.Add("RHD");
        if (Dealer) roles.Add("dealer");
        if (Second) roles.Add("2nd");
        if (Third) roles.Add("3rd");
        if (Fourth) roles.Add("4th");

        if (roles.Any()) return "Role: " + string.Join(", ", roles);
        return "Role: Any";
      }
    }

    private void SetRoleValue_(ref bool field, bool value, [CallerMemberName] string property = null)
    {
      SetValue_(ref field, value, property, nameof(Header));
    }
  }
}