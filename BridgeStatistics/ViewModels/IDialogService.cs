﻿namespace BridgeStatistics.ViewModels
{
  public interface IDialogService
  {
    bool? ShowDialog(DialogViewModel dialogVm);
  }
}