﻿namespace BridgeStatistics.ViewModels
{
  using System.ComponentModel;
  using System.Runtime.CompilerServices;

  using BridgeStatistics.ViewModels.Annotations;

  public abstract class ViewModel : INotifyPropertyChanged
  {
    public event PropertyChangedEventHandler PropertyChanged;

    [NotifyPropertyChangedInvocator]
    protected virtual void OnPropertyChanged_([CallerMemberName] string propertyName = null)
    {
      PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }

    protected void SetValue_<T>(ref T field, T value, string property, params string[] additionalProperties)
    {
      if (Equals(field, value)) return;
      field = value;
      OnPropertyChanged_(property);
      foreach (var aProperty in additionalProperties)
      {
        OnPropertyChanged_(aProperty);
      }
    }
  }
}