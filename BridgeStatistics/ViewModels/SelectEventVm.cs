namespace BridgeStatistics.ViewModels
{
  using System.Collections.Generic;
  using System.Linq;
  using System.Text.RegularExpressions;

  using BridgeStatistics.Model.WriteModel;

  public class SelectEventVm : DialogViewModel
  {
    private readonly List<Event> allEvents_;
    private List<Event> filteredEvents_;
    private string filter_ = string.Empty;

    public SelectEventVm(List<Event> events)
    {
      allEvents_ = events;
      filteredEvents_ = new List<Event>(allEvents_);
    }

    public override string Title => "Select events(s)";

    public string Filter
    {
      get => filter_;
      set
      {
        if (value == filter_) return;
        filter_ = value;
        Events = allEvents_.Where(e => Regex.IsMatch(e.DisplayName, Filter)).ToList();
        OnPropertyChanged_(nameof(Filter));
      }
    }

    public List<Event> Events
    {
      get => filteredEvents_;
      private set
      {
        if (value == filteredEvents_) return;
        filteredEvents_ = value;
        OnPropertyChanged_(nameof(Events));
      }
    }

    public List<Event> SelectedItems { get; private set; }

    public Command OkCommand => new Command(OnOkClicked_);
    public Command OkAllCommand => new Command(OnOkAllClicked_);
    public Command CancelCommand => new Command(() => Close(false));

    private void OnOkClicked_(object parameter)
    {
      SelectedItems = ((IEnumerable<object>)parameter).Cast<Event>().ToList();
      Close(true);
    }

    private void OnOkAllClicked_()
    {
      SelectedItems = Events;
      Close(true);
    }
  }
}