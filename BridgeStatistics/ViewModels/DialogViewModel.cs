﻿namespace BridgeStatistics.ViewModels
{
  using System;

  public abstract class DialogViewModel : ViewModel
  {
    public event EventHandler Closed;

    public static IDialogService DialogService { get; set; }
    public abstract string Title { get; }
    public bool IsClosing { get; private set; }
    public bool IsClosed { get; private set; }
    public bool? Result { get; private set; }

    public bool? ShowDialog()
    {
      return DialogService.ShowDialog(this);
    }

    public void Close(bool? result)
    {
      IsClosing = true;

      Result = result;
      OnClosed_();

      IsClosing = false;
    }

    protected virtual void OnClosed_()
    {
      IsClosed = true;
      Closed?.Invoke(this, EventArgs.Empty);
    }
  }
}