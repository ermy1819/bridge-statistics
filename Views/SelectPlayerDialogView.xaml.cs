﻿namespace BridgeStatistics.Views
{
  using System.Windows.Controls;

  /// <summary>
  /// Interaction logic for SelectPlayerDialogView.xaml
  /// </summary>
  public partial class SelectPlayerDialogView : UserControl
  {
    public SelectPlayerDialogView()
    {
      InitializeComponent();
    }
  }
}
