﻿namespace BridgeStatistics.Views
{
  using System.Windows.Controls;

#pragma warning disable S110 // Inheritance tree of classes should not be too deep
  /// <summary>
  /// Interaction logic for Workspace.xaml
  /// </summary>
  public partial class Workspace : UserControl
#pragma warning restore S110 // Inheritance tree of classes should not be too deep
  {
    public Workspace()
    {
      InitializeComponent();
    }
  }
}
