﻿namespace BridgeStatistics.Views
{
  using System;
  using System.Windows;
  using BridgeStatistics.ViewModels;

  public class DialogService : IDialogService
  {
    public bool? ShowDialog(DialogViewModel dialogVm)
    {
      var window = CreateWindow_(dialogVm);

      void ClosedHandler(object s, EventArgs e)
      {
        dialogVm.Closed -= (EventHandler)ClosedHandler;
        window.Close();
      }

      dialogVm.Closed += (EventHandler)ClosedHandler;

      // When the window is closing
      window.Closing += (s, e) =>
        {
          // Is the dialog closed from within the vm or from the window...
          if (dialogVm.IsClosing) return;
          dialogVm.Closed -= (EventHandler)ClosedHandler;
          dialogVm.Close(null);
          if (dialogVm.IsClosed) return;
          e.Cancel = true;
          dialogVm.Closed += (EventHandler)ClosedHandler;
        };

      window.Activate();
      window.ShowDialog();

      return dialogVm.Result;
    }

    private Window CreateWindow_(DialogViewModel content)
    {
      return new Window
               {
                 Title = content.Title,
                 Content = content,
                 ShowInTaskbar = false,
                 Owner = GetOwner_(),
                 SizeToContent = SizeToContent.WidthAndHeight,
                 MaxHeight = SystemParameters.WorkArea.Height,
                 MaxWidth = SystemParameters.WorkArea.Width,
                 ResizeMode = ResizeMode.CanResizeWithGrip,
                 WindowStartupLocation = WindowStartupLocation.CenterOwner
               };
    }

    private Window GetOwner_()
    {
      return Application.Current.MainWindow.Visibility == Visibility.Collapsed ? null : Application.Current.MainWindow;
    }
  }
}
