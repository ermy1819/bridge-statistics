namespace PbnParser
{
  using BridgeStatistics.Model.WriteModel;

  internal class DirectionParser
  {
    public PlayerDirection Parse(string value)
    {
      switch (value)
      {
        case "E":
          return PlayerDirection.East;
        case "S":
          return PlayerDirection.South;
        case "W":
          return PlayerDirection.West;
        case "N":
          return PlayerDirection.North;
        default:
          return default(PlayerDirection);
      }
    }
  }
}