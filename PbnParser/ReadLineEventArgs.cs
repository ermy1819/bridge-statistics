﻿namespace PbnParser
{
  using System;

  internal class ReadLineEventArgs : EventArgs
  {
    public ReadLineEventArgs(string line)
    {
      Line = line;
    }

    public string Line { get; }
  }
}